<?php
class ImageClass
{
    // property declaration
    public $baseDir;

    private $source;
    private $target;

    private $originWidth;
    private $originHeight;
    private $extension;
    private $tag;

    function __construct($filename) {
        $filename = $this->baseDir . $filename;

        if(!file_exists($filename)) {
            throw new Exception("$filename not found!");
        }

        if($info === false) {
            throw new Exception("unknow type!");
        }

        $info = getimagesize($filename);

        list($this->originWidth, $this->originHeight, $MIME) = $info;
        $this->extension = image_type_to_extension($MIME, false);

        switch(strtolower($this->extension)) {
            case 'png':
                $this->source = imagecreatefrompng($filename);
                break;
            case 'jpeg':
                $this->source = imagecreatefromjpeg($filename);
                break;
            case 'gif':
                $this->source = imagecreatefromgif($filename);
                break;
            default:
                throw new Exception("unsupport image type!");
        }
    }

    function __destruct() {
        imagedestroy($this->source);
        imagedestroy($this->target);
    }

    // method declaration
    public function resize($width, $height, $maintenanceRatio = true) {
        if($width == 0) {
            $width  = $this->originWidth;
        }

        if($height == 0) {
            $height = $this->originHeight;
        }

        if($maintenanceRatio) {
            // Calculate ratio of desired maximum sizes and original sizes.
            $widthRatio = $width / $this->originWidth;
            $heightRatio = $height / $this->originHeight;

            // Ratio used for calculating new image dimensions.
            $ratio = min($widthRatio, $heightRatio);

            // Calculate new image dimensions.
            $width  = (int) $this->originWidth  * $ratio;
            $height = (int) $this->originHeight * $ratio;
        }

        $this->target = imagecreatetruecolor($width, $height);

        imagecopyresampled($this->target, $this->source, 
                           0, 0, 0, 0, $width, $height, 
                           $this->originWidth, $this->originHeight);
    }

    public function display() {
 
        switch(strtolower($this->extension)) {
            case 'png':
                header('Content-Type: image/png');
                imagepng($this->target);
                break;
            case 'jpeg':
                header('Content-Type: image/jpeg');
                imagejpeg($this->target);
                break;
            case 'gif':
                // Output the image to browser
                header('Content-Type: image/gif');
                imagegif($this->target);
                break;
            default:
                throw new Exception("unsupport image type!");
        }
    }

}
?>
